GO_FLAGS := -a -ldflags '-w -extldflags "-static"'

build-static:
	CGO_ENABLED=0 go build $(GO_FLAGS)

build:
	go build
