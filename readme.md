# CI middleware

Install go 1.16 and run

```
make
```

You may need to install the dependencies first, which go will tell you about hopefully.

To run (note the initial whitespace which ensures that the secret will not be logged in your shell history file)

```
 GITLAB_SECRET=xxx ./example
```
