package main

import (
    "os"
    "log"
    "net/http"
    "fmt"
    // for listening to webhooks
    gitlab "github.com/go-playground/webhooks/v6/gitlab"

    // for interacting with the api
    gogitlab "github.com/xanzy/go-gitlab"
)

type Config struct {
    gitlab_secret string
    trigger_token string
    slurm_cancel_project string
    client *gogitlab.Client
}

func configFromEnv() *Config {
    gitlab_secret := os.Getenv("GITLAB_SECRET")
    trigger_token := os.Getenv("TRIGGER_TOKEN")
    slurm_cancel_project := os.Getenv("SLURM_CANCEL_PROJECT")

    if len(gitlab_secret) == 0 {
        log.Fatal("GITLAB_SECRET not set")
    }

    if len(trigger_token) == 0 {
        log.Fatal("TRIGGER_TOKEN not set")
    }

    if len(slurm_cancel_project) == 0 {
        log.Fatal("SLURM_CANCEL_PROJECT not set")
    }

    client, err := gogitlab.NewClient("")

    if err != nil {
		log.Fatalf("Couldn't create a Gitlab client")
	}

    c := Config{
        gitlab_secret,
        trigger_token,
        slurm_cancel_project,
        client,
    }

    return &c
}

func handle_pipeline_event(pipeline gitlab.PipelineEventPayload, c *Config) {
    // TODO: filter by known projects ?
    pipeline_id := pipeline.ObjectAttributes.ID
    project_name := pipeline.Project.Name
    status := pipeline.ObjectAttributes.Status

    if (status == "success" || status == "failed" || status == "canceled") {
        log.Printf("Dropping `%v` allocation pipeline `%v`\n", project_name, pipeline_id)
        opt := &gogitlab.RunPipelineTriggerOptions{
            Ref: gogitlab.String("main"),
            Token: gogitlab.String(c.trigger_token),
            Variables: map[string]string{
                "PIPELINE_ID": fmt.Sprintf("%d", pipeline_id),
            },
        }
        _, _, err := c.client.PipelineTriggers.RunPipelineTrigger(c.slurm_cancel_project, opt)

        if err != nil {
            log.Printf("Couldn't trigger the slurm cancel pipeline")
        }
    } else {
        log.Printf("Ignoring `%v` pipeline status = `%v`", project_name, status)
    }
}

func main() {
    config := configFromEnv()

    hook, _ := gitlab.New(gitlab.Options.Secret(config.gitlab_secret))

    http.HandleFunc("/gitlab", func(w http.ResponseWriter, r *http.Request) {
        payload, err := hook.Parse(r, gitlab.PipelineEvents)

        // we could do some proper logging?
        if err != nil { return }

        switch payload.(type) {
        case gitlab.PipelineEventPayload:
            handle_pipeline_event(payload.(gitlab.PipelineEventPayload), config)
        }
    })

    http.ListenAndServe(":3131", nil)
}
